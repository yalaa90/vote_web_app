import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import {HttpModule} from '@angular/http'
import {Route,  Router, RouterModule, Routes } from '@angular/router';
import {CategoriesService} from './vote/auth/categories.service';
import { AppComponent }  from './app.component';
import {Card} from './vote/card/card.component';
import {CardService} from './vote/card/card.service';
import {RegistrationComponent} from './vote/auth/registration.component';
import {RegistrationService} from './vote/auth/registration.service';
import {MdCard,MdCardHeader,MdCardTitle,MdCardSubtitle,MdMenu,MdIcon} from '@angular/material';
import {CategoriesComponent} from './vote/auth/categories.component';
import {FindFriendsComponent} from './vote/auth/findFriends.component';
import {AngularFireModule} from 'angularfire2'
import {AccKit} from './vote/acckit/acckit.component';
export const fireBaseConfig = {
  apiKey: 'AIzaSyAXc6Boy5V1zkmKQ50t2H7BQbdu6dSHcpY',
  authDomain: 'vote-avenue-dev-1.firebaseio.com',
  databaseURL: 'https://vote-avenue-dev-1.firebaseio.com',
  storageBucket: ''
};
const routes:Routes =[

    {path:'registration', component:RegistrationComponent},
    {path:'categories/:id',component:CategoriesComponent} ]
@NgModule({
  imports: [ BrowserModule , FormsModule ,ReactiveFormsModule,HttpModule,AngularFireModule.initializeApp(fireBaseConfig),
    RouterModule.forRoot(routes)
     ],
  declarations: [
    AppComponent,
    Card,MdCard,MdCardHeader,MdCardTitle,MdCardSubtitle,MdMenu,MdIcon,RegistrationComponent,CategoriesComponent,FindFriendsComponent
  ,AccKit
  ],
  providers:[CardService,RegistrationService,CategoriesService],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
