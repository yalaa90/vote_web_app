
import {Injectable} from '@angular/core';
import {Http, RequestOptionsArgs, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Poll} from '../poll';
@Injectable()
export class CardService{

    constructor(private http:Http) {}

 //   public  getPoll(pollId) :Poll;

    public getAllPoll():Observable<Poll>  {
        return (this.http.get('https://httpbin.org/get',null).map((res :Response)=>{ return res.json()}));

    }
    public getAllPollSub():Poll[]{
        let data:Poll[]=[];

        this.getAllPoll().subscribe(res=>{
          data.push(res)}
        );

        return data;

    }
}