"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var material_1 = require("@angular/material");
var card_service_1 = require("./card.service");
var Card = (function () {
    function Card(cardService) {
        this.cardService = cardService;
        this.cardForm = new forms_1.FormGroup({});
        this.today = Date.now();
        this.tags = ['footBall', 'world_wide'];
        var data = cardService.getAllPollSub();
    }
    return Card;
}());
__decorate([
    core_1.ViewChild(material_1.MdMenuTrigger),
    __metadata("design:type", material_1.MdMenuTrigger)
], Card.prototype, "trigger", void 0);
Card = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'card',
        templateUrl: 'card.component.html'
    }),
    __metadata("design:paramtypes", [card_service_1.CardService])
], Card);
exports.Card = Card;
//# sourceMappingURL=card.component.js.map