import {Component} from '@angular/core';
import {FormBuilder,Validators } from '@angular/forms';
import {CategoriesService} from './categories.service';
import  {Router} from '@angular/router';
@Component({
    moduleId:module.id,
    selector:'categories',
    templateUrl:'categories.component.html'
})
export class  CategoriesComponent {

  constructor(private _categoriesService:CategoriesService,fb:FormBuilder,private router:Router ){

  }
  
  data;
  submit() {
    this._categoriesService.submit(this.data);
  }

}
