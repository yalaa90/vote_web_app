

import {Component} from '@angular/core';
import {FormBuilder,Validators } from '@angular/forms';
import {RegistrationService} from './registration.service';
import  {Router} from '@angular/router';
declare var Auth0Lock;
@Component({
    moduleId:module.id,
    selector:'reg',
    templateUrl:'registration.component.html'

})
export class RegistrationComponent {
  registerForm ;
    constructor(private _registrationService:RegistrationService,fb:FormBuilder,private router:Router ){

      this.registerForm= fb.group({
        name: ['',Validators.required],
        userName: ['',Validators.required],
        email:['',[Validators.required]],
        mobileNumber:[''],
        gender: [''],
        location: [''],
        birthDate: ['']
      })
    }

  lock = new Auth0Lock('hYVlHsjg3haGcoLyKExb3pMIs2rel6DG', 'yalaa90.auth0.com');

  login() {
    debugger;
    var hash = this.lock.parseHash();
    if (hash) {
      if (hash.error)
        console.log('There was an error logging in', hash.error);
      else
        this.lock.getProfile(hash.id_token, function(err, profile) {
          if (err) {
            console.log(err);
            return;
          }
          localStorage.setItem('profile', JSON.stringify(profile));
          localStorage.setItem('id_token', hash.id_token);
        });
    }
  }


  submitForm(){
        let service = this._registrationService.submit(this.registerForm);
        service.then(i=> {
          let id = i.path.o[1];
          this.router.navigate(['/categories',id])

        })

    }
}
