/**
 * Created by yahia on 1/12/2017.
 */
import {Injectable} from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Injectable()
export class CategoriesService {

  constructor(private af: AngularFire){}

  categories:FirebaseListObservable <any[]>;

  public submit(form:any) {

    debugger;
    this.categories=this.af.database.list('/categories');
    let model=form._value ;
    return this.categories.push(model)

  }


}
