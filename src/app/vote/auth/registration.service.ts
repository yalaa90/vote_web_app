import {Injectable} from '@angular/core';


import { AngularFire, FirebaseListObservable } from 'angularfire2';
@Injectable()
export class RegistrationService {

    constructor(private af: AngularFire){}

    users:FirebaseListObservable <user[]>;
    public submit(form:any) {

        debugger;
        this.users=this.af.database.list('/users');
        let model=form._value ;
       return this.users.push(model)

    }

}

export interface user {
    name: string;
    userName: string;
    email: string;
    mobileNumber: string;
    gender: string;
    location: string;
    birthDate: string;

}
