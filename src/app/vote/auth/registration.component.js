"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var registration_service_1 = require("./registration.service");
var RegistrationComponent = (function () {
    function RegistrationComponent(_registrationService) {
        this._registrationService = _registrationService;
        this.registerForm = new forms_1.FormGroup({
            name: new forms_1.FormControl(),
            userName: new forms_1.FormControl(),
            email: new forms_1.FormControl(),
            mobileNumber: new forms_1.FormControl(),
            gender: new forms_1.FormControl(),
            location: new forms_1.FormControl(),
            birthDate: new forms_1.FormControl()
        });
    }
    RegistrationComponent.prototype.submitForm = function () {
        this._registrationService.submit(this.registerForm);
    };
    return RegistrationComponent;
}());
RegistrationComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'reg',
        templateUrl: 'registration.component.html'
    }),
    __metadata("design:paramtypes", [registration_service_1.RegistrationService])
], RegistrationComponent);
exports.RegistrationComponent = RegistrationComponent;
//# sourceMappingURL=registration.component.js.map