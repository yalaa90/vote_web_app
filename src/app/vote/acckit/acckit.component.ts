/**
 * Created by yahia on 1/22/2017.
 */
import  {Component,Injector} from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire,AuthMethods,AuthProviders} from 'angularfire2'
declare var AccountKit:any;
@Component({
  selector: 'acckit',
  templateUrl: 'acckit.component.html'
})
export class AccKit {

  message:string ="hello";
  constructor(private _http:Http, private af:AngularFire) {

    AccountKit.init(
      {
        appId: '831150530357606',
        state: "ABCBCAABCCBCA!@#$!%##!",
        version: "v1.1",
        debug: true
      }
    );


  }
/* sendReuqest(response) {
     this.http.post('localhost:8077auth/facebook', response, null).subscribe(
     i=> console.log("after post" + i)
   );
 }*/


  phone_btn_onclick() {
    /*var country_code = document.getElementById("country_code").value;
     var ph_num = document.getElementById("phone_num").value;
     */

    AccountKit.login('PHONE',
      {countryCode: '+20', phoneNumber: ''}, // will use default values if this is not specified
      (response)=>{
        console.log(response)
        if (response.status === "PARTIALLY_AUTHENTICATED") {
          this._http.post('http://localhost:8077/auth/facebook', response, null).subscribe(
            i=> {

              this.af.auth.login(JSON.parse(i.text()).firebaseToken,{
                provider: AuthProviders.Custom,
                method: AuthMethods.CustomToken
              }).then(i=> console.log(i));
            });


          /*document.getElementById("code").value = response.code;
           document.getElementById("csrf_nonce").value = response.state;
           document.getElementById("my_form").submit();*/
        }
        else if (response.status === "NOT_AUTHENTICATED") {

          // handle authentication failure
        }
        else if (response.status === "BAD_PARAMS") {
          // handle bad parameters
        }
      });
  }

}
